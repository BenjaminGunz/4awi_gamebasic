package at.bgu.basics.car;

public class Car {
	private String color;
	private float maxSpeed;
	private float basicPrice;
	private double basicUsage;
	private float mileage;

	private Producer producer;

	public Car(String color, float maxSpeed, float basicPrice, double basicUsage, Producer producer, float mileage) {
		super();
		this.color = color;
		this.maxSpeed = maxSpeed;
		this.basicPrice = basicPrice;
		this.basicUsage = basicUsage;
		this.producer = producer;
		this.mileage = mileage;
	}

	public float getMileage() {
		return mileage;
	}

	public void setMileage(float mileage) {
		this.mileage = mileage;
	}

	public Producer getProducer() {
		return producer;
	}

	public void setProducer(Producer producer) {
		this.producer = producer;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public float getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public float getBasicPrice() {
		return basicPrice;
	}

	public void setBasicPrice(float basicPrice) {
		this.basicPrice = basicPrice;
	}

	public double getBasicUsage() {
		return basicUsage;
	}

	public void setBasicUsage(float basicUsage) {
		this.basicUsage = basicUsage;
	}

	public double getUsage() {
		if (this.mileage < 50000) {
			return this.basicUsage;
		}
		if (this.mileage > 50000) {

			this.basicUsage = (this.basicUsage * 1.098);

		}

		return this.basicUsage;

	}

}
