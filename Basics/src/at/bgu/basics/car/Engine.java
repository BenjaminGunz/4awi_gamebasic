package at.bgu.basics.car;

public class Engine {
	private float horesPower;
	private String engineType;
	
	public Engine(float horesPower, String engineType) {
		super();
		this.horesPower = horesPower;
		this.engineType = engineType;
	}

	public float getHoresPower() {
		return horesPower;
	}

	public void setHoresPower(float horesPower) {
		this.horesPower = horesPower;
	}

	public String getEngineType() {
		return engineType;
	}

	public void setEngineType(String engineType) {
		this.engineType = engineType;
	}
	
	
	
	
	
	
	
	
	
	
}
