package at.bgu.basics.car;

import java.util.ArrayList;
import java.util.List;

public class Person {
	private List<Car> cars;

	Producer p1 = new Producer("Gunz", "Austria", 5);

	public Person() {
		this.cars = new ArrayList<>();

		Car c1 = new Car("blue", 7, 500, 15, p1, 60000);
		Car c2 = new Car("green", 8, 600, 10, p1, 40000);

		c2.getColor();

		this.cars.add(c1);
		this.cars.add(c2);

	}

	public void addCars(Car car) {

		this.cars.add(car);
	}

	public void printoutCars() {
		
		for (Car car : cars) {
			System.out.println(car.getBasicPrice());
		}
	
	}
	
	public List<Car> getCars(){
		return cars;
		
		
	}
}