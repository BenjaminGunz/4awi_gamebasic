package at.bgu.basics.remote;

public class Battery {
	private int ChargingStatus;
	private static final int POWER_LEVEL=50;
	

	public Battery(int ChargingStatus) {
		super();
		this.ChargingStatus = ChargingStatus;
	}

	public int getChargingStatus() {
		return ChargingStatus;
	}

	public void setChargingStatus(int chargingStatus) {
		ChargingStatus = chargingStatus;
	}

	public boolean hasPower() {
		if (ChargingStatus < POWER_LEVEL) {
			return false;
		} else {
			return true;
		}

	}

}
