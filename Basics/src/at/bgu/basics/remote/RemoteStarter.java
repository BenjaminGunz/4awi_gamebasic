package at.bgu.basics.remote;

public class RemoteStarter {

	public static void main(String[] args) {
		Battery b1 = new Battery(60);
		
		Remote r1 = new Remote(false, b1);
		
		 
		System.out.println(r1.isOn());
		
		r1.turnOn();
		
		System.out.println(r1.isOn());
		
		r1.turnOff();
		
		System.out.println(r1.isOn());
		
		System.out.println(b1.getChargingStatus());
		
		System.out.println(b1.hasPower());
	}		
}
