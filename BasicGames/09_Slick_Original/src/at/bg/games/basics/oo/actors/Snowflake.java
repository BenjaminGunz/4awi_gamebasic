package at.bg.games.basics.oo.actors;

import java.util.Random;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import at.bg.games.basics.oo.Actor;

public class Snowflake implements Actor {

	private double x, y , speed;
	public enum size{small, medium, big}
	private int radius;
	
	public Snowflake(double speed, size s) {
		setRandomPosition();
		this.speed = speed;
	
		
		if (s == size.small) {
			radius = 2;
		}else if (s == size.medium) {
			radius = 3;
		}else {
			radius = 5;
		}
		
	}
	 public void setRandomPosition() {
		 Random r = new Random();
		 this.y = r.nextInt(600) - 600;
		 this.x = r.nextInt(800);
	 }
	 
	 public void update(GameContainer container, int delta) {
		 this.y += delta * speed;
		 
		 if (this.y>800) {
			 setRandomPosition();
		 }
	 }
	 
	 public void render(Graphics g) {
		 
		 g.fillOval((int)this.x, (int)this.y, radius *2, radius *2);
		 
	 }
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	
	public int getRadius() {
		return radius;
	}
	public void setRadius(int radius) {
		this.radius = radius;
	}
	 
}
