package at.bg.games.basics.oo.actors;

import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import at.bg.games.basics.oo.Actor;

public class ShootingStar implements Actor {

	private double x, y, speed;
	private int radius;

	public void setRandomPosition() {
		 Random r = new Random();
		 this.y = r.nextInt(600) - 600;
		 this.x = r.nextInt(800);
	 }
	
	public void update(GameContainer container, int delta) {
		
		this.x += delta * (speed/10);
		this.y += delta * (speed/10);
		
		
		
		
	}

	
	public void render(Graphics g) {
		g.setColor(Color.red);
		g.fillRoundRect((int) this.x, (int) this.y, 100, 100, this.radius);
		g.setColor(Color.white);
	}

	public ShootingStar(double x, double y, double speed, int radius) {
		super();
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.radius = radius;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

}
