package at.bg.games.basics.oo.move;

public interface MoveStrategy {
	public void update(int delta);
	public double getX();
	public double getY();

}
