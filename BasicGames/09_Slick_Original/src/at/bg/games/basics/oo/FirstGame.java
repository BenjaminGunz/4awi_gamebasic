package at.bg.games.basics.oo;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import at.bg.games.basics.oo.actors.HTLOval;
import at.bg.games.basics.oo.actors.HTLRectangle;
import at.bg.games.basics.oo.actors.HTLRound;
import at.bg.games.basics.oo.actors.ShootingStar;
import at.bg.games.basics.oo.actors.Snowflake;
import at.bg.games.basics.oo.actors.Starship;

public class FirstGame extends BasicGame {
	private List<Actor> actors;
	private Starship Starship;
	

	public FirstGame() {
		super("FirstGame");

	}

	@Override
	public void render(GameContainer gameContainer, Graphics g) throws SlickException {
		for(Actor actor : actors) {
			actor.render(g);
		}
		
	}

	@Override
	public void init(GameContainer gameContainer) throws SlickException {
		this.actors = new ArrayList<>();
		addSnowflakes();
		addOtherObjects();
		this.Starship = new Starship(20, 20);

	}

	private void addOtherObjects() {
		actors.add(new HTLRectangle(0, 0, 100, 100, 0));
		actors.add(new HTLOval(0, 250, 100, 100, 0));
		actors.add(new HTLRound(350, -100, 100, 100));
		actors.add(new ShootingStar(10, 10, 1, 100));
		actors.add(new Starship(20,20));
	}

	private void addSnowflakes() {
		for (int i = 0; i < 50; i++) {
			actors.add(new Snowflake(0.4, Snowflake.size.big));
		}
		for (int i = 0; i < 50; i++) {
			actors.add(new Snowflake(0.2, Snowflake.size.medium));
		}
		for (int i = 0; i < 50; i++) {
			actors.add(new Snowflake(0.1, Snowflake.size.small));
		}
	}

	@Override
	public void update(GameContainer gameContainer, int delta) throws SlickException {
		for (Actor actor : actors) {
			actor.update(gameContainer,delta);
		}
		
		
		

	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new FirstGame());
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
