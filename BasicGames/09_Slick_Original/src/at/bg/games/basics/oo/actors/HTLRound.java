package at.bg.games.basics.oo.actors;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import at.bg.games.basics.oo.Actor;

public class HTLRound implements Actor{
	private double x;
	private double y;
	private int width;
	private int height;
	

	

	
	public HTLRound(double x, double y, int width, int height) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	

	public void update(GameContainer container, int delta){
		this.y ++;
		if(this.y >= 600) {
			this.y = -100;
		}
		
	}

	public void render(Graphics g) {
		g.setColor(Color.blue);
		g.drawRect((int)this.x,(int)this.y, this.width, this.height);
		g.setColor(Color.white);

	}

}
