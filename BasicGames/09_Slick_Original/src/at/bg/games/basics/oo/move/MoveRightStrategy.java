package at.bg.games.basics.oo.move;

public class MoveRightStrategy implements MoveStrategy {
	private double x, y;

	public MoveRightStrategy(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	public void update(int delta) {
		this.x++;

	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

}
