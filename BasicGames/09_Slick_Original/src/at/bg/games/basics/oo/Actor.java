package at.bg.games.basics.oo;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public interface Actor {
	public void update(GameContainer container, int delta);
	public void render(Graphics graphics);
}
