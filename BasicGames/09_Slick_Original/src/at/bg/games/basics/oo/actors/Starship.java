package at.bg.games.basics.oo.actors;






import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.AppGameContainer;





import at.bg.games.basics.oo.Actor;

public class Starship implements Actor{
	private double x, y;
	private int width, height;
	private Image image;
	private boolean moveleft = false;
	private boolean moveright = false;
	private boolean moveup = false;
	private boolean movedown = false;
	

	public Starship(int x, int y)
	{
		super();
		this.x = x;
		this.y = y;
		this.width = 100;
		this.height = 50;
		try {
			this.image = new Image("testdata/spaceship.png");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void update(GameContainer container, int delta)
	{
        moveleft = container.getInput().isKeyDown(Input.KEY_LEFT);
        moveright = container.getInput().isKeyDown(Input.KEY_RIGHT);
        moveup = container.getInput().isKeyDown(Input.KEY_UP);
        movedown = container.getInput().isKeyDown(Input.KEY_DOWN);
       
		
		if(moveleft == true)
		{
			this.x--;
		}
		if(moveright == true)
		{
			this.x++;
		}
		if(moveup == true)
		{
			this.y--;
		}
		if(movedown == true)
		{
			this.y++;
		}
		
	}

	public void render(Graphics g)
	{
		this.image.draw((int)this.x, (int)this.y);
	}


}