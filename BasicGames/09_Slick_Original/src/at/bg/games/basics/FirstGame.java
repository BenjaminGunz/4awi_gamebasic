package at.bg.games.basics;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;


public class FirstGame extends BasicGame {
	private int moveDirection = 0; // 0 rechts, 1 unten, 2 links, 3 oben
	private int moveDirectionOval = 0; // 0 rechts 1 links
	private static final double SPEED = 1;
	private int xR;
	private int yR;
	private int xO = 0;
	private int yO = 250;
	private int xK = 350;
	private int yK = 0;
	
	
	public FirstGame() {
		super("FirstGame");
		
	}

	@Override
	public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
		graphics.drawRect(this.xR, this.yR, 100, 100);
		graphics.drawOval(this.xO, this.yO, 100, 100);
		graphics.drawRoundRect(this.xK, this.yK, 100, 100, 40);
	}

	@Override
	public void init(GameContainer gameContainer) throws SlickException {
		

	}

	@Override
	public void update(GameContainer gameContainer, int delta) throws SlickException {
		if(moveDirection == 0) {
			this.xR += (double) delta * SPEED;
			if(xR > 700) {
				this.moveDirection = 1;
			}
		}
		
		
		if(moveDirection == 1) {
			this.yR += (double) delta * SPEED;
			if(yR > 500) {
				this.moveDirection = 2;
			}
		}
		
		if(moveDirection == 2) {
			this.xR -= (double) delta * SPEED;
			if(xR < 0) {
				this.moveDirection = 3;
			}
		}
		
		if(moveDirection == 3) {
			this.yR -= (double) delta * SPEED;
			if(yR < 0) {
				this.moveDirection = 0;
			}
		}
		
		if(moveDirectionOval == 0) {
			this.xO += (double) delta * SPEED;
			if(xO > 700) {
				this.moveDirectionOval = 1;
			}
		}
		if(moveDirectionOval == 1) {
			this.xO -= (double) delta * SPEED;
			if(xO < 0) {
				this.moveDirectionOval = 0;
			}
		}
		this.yK ++;
		if(this.yK >= 600) {
			this.yK = -100;
		}
		
		
	}
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new FirstGame());
			container.setDisplayMode(800,600,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
