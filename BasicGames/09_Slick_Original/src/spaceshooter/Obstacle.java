package spaceshooter;


import java.util.Random;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;

public class Obstacle {

	
	private float x, y;
	private float speed;
	private Image image;
	private Shape shape;
	private boolean hasCollision;
	
	public Obstacle(float x, float y, float height, float width, float speed) throws SlickException {
		super();
		this.x = x;
		this.y = y;
		this.speed = speed;
		image= new Image("testdata/comet-310331_340.png");
		this.shape = new Circle(this.x + 22, this.y + 20, 10);
		this.hasCollision=false;
	}

	public void update(int millisSinceLastCall) {
		if(this.y > 730 )
		{
			this.y = -100;
			Random random = new Random();
			int x = random.nextInt(300);
			this.x = x;
		}
		else
		{
			this.y = this.y + (this.speed * millisSinceLastCall);
		}
		this.shape.setX(this.x + 12);
		this.shape.setY(this.y + 30);
	}

	public void render(Graphics g) {
		
		image.draw(this.x, this.y);
		
	}

	public Shape getShape() {
		return shape;
	}

	public void setShape(Shape shape) {
		this.shape = shape;
	}

	public boolean isHasCollision() {
		return hasCollision;
	}

	public void setHasCollision(boolean hasCollision) {
		this.hasCollision = hasCollision;
	}
	

	
}




