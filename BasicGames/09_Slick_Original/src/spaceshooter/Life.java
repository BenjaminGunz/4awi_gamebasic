package spaceshooter;


import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Life {
	
	private Image image;
	private int x,y;

	public Life(int x, int y) throws SlickException {
		super();
		this.x = x;
		this.y = y;
		image = new Image("");
	}
	
	public void render(Graphics g) {
		image.draw(this.x, this.y);
		
	}
	
	

}