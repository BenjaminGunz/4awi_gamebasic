package spaceshooter;



import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class ScoreBoard {

	private int score;
	private int timer;
 
	public void render(GameContainer gc, Graphics g) throws SlickException {
		g.drawString("Punkte : " + score, 150, 10);
	}

	public ScoreBoard() {
		super();
		this.score = 0;
		this.timer = 0;

	}

	public void update(GameContainer gc, int millisSinceLastCall) throws SlickException {
		
		this.timer += millisSinceLastCall;
		if (this.timer>500){
			System.out.println("Second");
			this.score = this.score + 1;
			this.timer=0;
	
			
		}
		
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public ScoreBoard(int score, int x, int y) {
		super();
		this.score = score;
	}

}