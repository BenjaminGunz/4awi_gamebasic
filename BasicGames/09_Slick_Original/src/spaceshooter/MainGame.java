package spaceshooter;

import java.util.ArrayList;
import java.util.Random;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class MainGame extends BasicGame {
	private static final int INITIAL_LIFES = 3;
	public ArrayList<Obstacle> obstacles;
	public Spaceship Spaceship;
	public ScoreBoard ScoreBoard;
	public String gameover ;
	public ArrayList<Life> lifes;


	public boolean hasCollision = false;
	
	private boolean isKeyLeftPressed = false;
	private boolean isKeyRightPressed = false;
	private boolean isKeyUpPressed = false;
	private boolean isKeyDownPressed = false;
	
	public MainGame() {
		super("myGame");
		
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
		for (int i = 0; i < this.obstacles.size(); i++) {
			this.obstacles.get(i).render(g);
		}

		this.Spaceship.render(g);
		this.ScoreBoard.render(gc, g);

		if (this.lifes.size() > 0) {
			for (int i = 0; i < this.lifes.size(); i++) {
				this.lifes.get(i).render(g);
				}
			
			}
		if (this.lifes.size() == 0) {
			g.drawString("GameOver", 100, 100);
			gc.setPaused(true);
		}
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		this.obstacles = new ArrayList<>();
		this.Spaceship = new Spaceship(100, 100);

		for (int i = 0; i < 10; i++) {
			Random random = new Random();
			int x = random.nextInt(300);
			int y = random.nextInt(720);
			int width = random.nextInt(50);

			Obstacle Obstacle = new Obstacle(x, y, width, 5, 0.15f);
			this.obstacles.add(Obstacle);
			this.Spaceship.addCollisionPartner(Obstacle);
		}

		this.lifes = new ArrayList<>();

	

		for (int i = 0; i < INITIAL_LIFES; i++) {
			this.lifes.add(new Life(103 + (35 * i), 690));
		}

		this.ScoreBoard = new ScoreBoard();

	}

	@Override
	public void update(GameContainer gc, int millisSinceLastCall) throws SlickException {

		updateActors(gc, millisSinceLastCall);
		checkCollision();
		moveSpaceship(millisSinceLastCall);
		
	}

	private void updateActors(GameContainer gc, int millisSinceLastCall) throws SlickException {
		this.Spaceship.update(gc, millisSinceLastCall);
		this.ScoreBoard.update(gc, millisSinceLastCall);

		for (int i = 0; i < this.obstacles.size(); i++) {
			this.obstacles.get(i).update(millisSinceLastCall);
		}
	}

	private void moveSpaceship(int millisSinceLastCall) {
		if (this.isKeyLeftPressed) {
			this.Spaceship.moveLeft(millisSinceLastCall);
		}

		if (this.isKeyRightPressed) {
			this.Spaceship.moveRight(millisSinceLastCall);
		}

		if (this.isKeyDownPressed) {
			this.Spaceship.moveDown(millisSinceLastCall);
		}

		if (this.isKeyUpPressed) {
			this.Spaceship.moveUp(millisSinceLastCall);
		}
	}

	private void checkCollision() {
		this.hasCollision = this.Spaceship.getCollision();
		if (this.hasCollision) {
			System.out.println("col");
			this.Spaceship.setCollision(false);
			if (this.lifes.size() > 0) {
				this.lifes.remove(this.lifes.size() - 1);
			} 
			
		}
	}
	
	
	
	public void keyPressed(int key, char c) {
		if (key == 203 || c == 'a') {
			this.isKeyLeftPressed = true;
			this.isKeyRightPressed = false;
			this.isKeyDownPressed = false;
			this.isKeyUpPressed = false;
		}

		if (key == 205 || c == 'd') {
			this.isKeyLeftPressed = false;
			this.isKeyRightPressed = true;
			this.isKeyDownPressed = false;
			this.isKeyUpPressed = false;
		}

		if (key == 208 || c == 's') {
			this.isKeyLeftPressed = false;
			this.isKeyRightPressed = false;
			this.isKeyDownPressed = true;
			this.isKeyUpPressed = false;
		}

		if (key == 200 || c == 'w') {
			this.isKeyLeftPressed = false;
			this.isKeyRightPressed = false;
			this.isKeyDownPressed = false;
			this.isKeyUpPressed = true;
		}

	}

	public void keyReleased(int key, char c) {
		if (key == 203 || c == 'a') {
			this.isKeyLeftPressed = false;
		}

		if (key == 205 || c == 'd') {
			this.isKeyRightPressed = false;
		}

		if (key == 208 || c == 's') {
			this.isKeyDownPressed = false;
		}

		if (key == 200 || c == 'w') {
			this.isKeyUpPressed = false;
		}
	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame());
			container.setDisplayMode(300, 720, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}