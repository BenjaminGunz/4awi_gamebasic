package spaceshooter;

import java.util.ArrayList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class Spaceship {

	private float x, y;
	private Image image;
	private Shape shape1;
	private Shape shape2;
	private ArrayList<Obstacle> collisionPartner;
	private boolean hasCollision = false;
	private Obstacle actualObstacle;
	
	public Spaceship(float x, float y) throws SlickException {
		super();
		this.x = x;
		this.y = y;
		image = new Image("testdata/spaceship.png");
		this.shape1 = new Rectangle(this.x + 37, this.y + 14, 25, 75);
		this.shape2 = new Rectangle(this.x + 24, this.y + 50, 50, 15);
		this.collisionPartner = new ArrayList<>();
	}

	public void render(Graphics g) throws SlickException {
		image.draw(this.x, this.y);

		// g.draw(this.shape1);
		// g.draw(this.shape2);
	}

	public void update(GameContainer gc, int millisSinceLastCall) throws SlickException {

		for (Obstacle obstacle : collisionPartner) {
	
			
			if (!obstacle.isHasCollision() && this.shape1.intersects(obstacle.getShape())) {
				// System.out.println("Collision");
				this.hasCollision = true;
				obstacle.setHasCollision(true);
				
				//System.out.println("Collision");
			}
			if (!obstacle.isHasCollision() && this.shape2.intersects(obstacle.getShape())) {
				this.hasCollision = true;
				obstacle.setHasCollision(true);
			}
		}

	}

	public boolean getCollision() {
		return hasCollision;
	}

	public void setCollision(boolean collision) {
		this.hasCollision = collision;
	}

	public void moveLeft(int millisSinceLastCall) {

		if (this.x > -50) {
			this.x = this.x - (0.4f * millisSinceLastCall);
		}

		this.shape1.setX(this.x + 37);
		this.shape2.setX(this.x + 24);

		/*
		 * for (Obstacle obstacle : collisionPartner) { if
		 * (this.shape1.intersects(obstacle.getShape())) { //
		 * System.out.println("Collision"); collision = true; } if
		 * (this.shape2.intersects(obstacle.getShape())) { //
		 * System.out.println("Collision"); collision = true; } }
		 */

	}

	public void moveRight(int millisSinceLastCall) {

		if (this.x < 250) {
			this.x = this.x + (0.4f * millisSinceLastCall);
		}

		this.shape1.setX(this.x + 37);
		this.shape2.setX(this.x + 24);

		/*
		 * for (Obstacle obstacle : collisionPartner) { if
		 * (this.shape1.intersects(obstacle.getShape())) { //
		 * System.out.println("Collision"); collision = true; } if
		 * (this.shape2.intersects(obstacle.getShape())) { //
		 * System.out.println("Collision"); collision = true; } }
		 */

	}

	public void moveDown(int millisSinceLastCall) {

		if (this.y < 630) {
			this.y = this.y + (0.4f * millisSinceLastCall);
		}

		this.shape1.setY(this.y + 14);
		this.shape2.setY(this.y + 50);

		/*
		 * for (Obstacle obstacle : collisionPartner) { if
		 * (this.shape1.intersects(obstacle.getShape())) { //
		 * System.out.println("Collision"); collision = true; } if
		 * (this.shape2.intersects(obstacle.getShape())) { //
		 * System.out.println("Collision"); collision = true; } }
		 */
	}

	public void moveUp(int millisSinceLastCall) {

		if (this.y > 0) {
			this.y = this.y - (0.4f * millisSinceLastCall);
		}

		this.shape1.setY(this.y + 14);
		this.shape2.setY(this.y + 50);

		/*
		 * for (Obstacle obstacle : collisionPartner) { if
		 * (this.shape1.intersects(obstacle.getShape())) { //
		 * System.out.println("Collision"); collision = true; } if
		 * (this.shape2.intersects(obstacle.getShape())) { //
		 * System.out.println("Collision"); collision = true; } }
		 */
	}

	public void addCollisionPartner(Obstacle o) {
		this.collisionPartner.add(o);
	}

}
